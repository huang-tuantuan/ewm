package com.example.ewm;

import com.example.ewm.utils.QRCodeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;


@SpringBootTest
class EwmApplicationTests {

    @Resource
    private QRCodeUtil qrCodeUtil;

    @Test
    void contextLoads() throws Exception {

        qrCodeUtil.create("httstudy","F:\\基础二维码.jpg");
        qrCodeUtil.create("httstudy","F:\\带颜色的二维码.jpg",0xff0000,0xffff00);
        qrCodeUtil.create("httstudy","F:\\Logo.png","F:\\带logo的二维码.jpg",true);
        qrCodeUtil.create("httstudy","F:\\Logo.png","F:\\带颜色和logo的二维码.jpg",true,0xff0000,0xffff00);

        String str = qrCodeUtil.create("httstudy");
        String str2 = qrCodeUtil.create("httstudy",0xff0000,0xffff00);
        String str3 = qrCodeUtil.create("httstudy","F:\\Logo.png",true);
        String str4 = qrCodeUtil.create("httstudy","F:\\Logo.png",true,0xff0000,0xffff00);
        System.out.println(str);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);

        String result = qrCodeUtil.decode("F:\\基础二维码.jpg");
        String result2 = qrCodeUtil.decode("F:\\带颜色的二维码.jpg");
        String result3 = qrCodeUtil.decode("F:\\带logo的二维码.jpg");
        String result4 = qrCodeUtil.decode("F:\\带颜色和logo的二维码.jpg");
        System.out.println(result);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
    }

}
