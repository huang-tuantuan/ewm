# SpringBoot生成二维码完整工具类分享

#### 可以支持基础的黑白二维码、带颜色的二维码、带Logo的二维码、带颜色和Logo的二维码和解析二维码，可以生成具体的二维码文件或返回Base64。

#### 使用方法见单元测试

# 运行环境
####  SpringBoot2.7+Maven3.6.2+JDK1.8
